﻿/*Fecha de Inicio: 7 de octubre del 2015
  Fecha de Finalizacion: 6 de noviembre del 2015*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Collections;
using System.IO;

namespace SegundoProyecto
{
    class Program
    {
        
        /*Matriz*/
        public static int[,] matriz;
        public static int[,] matriz2;
        

        //Por medio de la libreria Stopwatch medire el tiempo de ejecucion del los algoritmos de ordenamiento
        static Stopwatch crono2, crono1, crono3,crono4;
       
        /*instancia para generar random*/
        public static Random randomGenerator = new Random();

        /*contado que indica la cantidad de figuras que se estan generando en la matriz*/
        public static int cantidadFigurasGeneradas=0;

        //dll import, funciona para utilizar los colores en la consola
        [DllImport("kernel32.dll")]
        public static extern bool SetConsoleTextAttribute(IntPtr hConsoleOutput,
          int wAttributes);
        [DllImport("kernel32.dll")]
        public static extern IntPtr GetStdHandle(uint nStdHandle);

        static string[] arregloPiezasTxt;
        static StreamReader leerTxt;

        public static ArrayList piezasArchivo = new ArrayList();



        public static void generacionPiezasInicial(int cantidadPiezas) {
            for (int i = 0;i<cantidadPiezas; i++)
            {
               
              int pieza =randomGenerator.Next(1, 13);
              piezasEnArchivo(pieza);
            }
            
        }

        public static void limpiarArchivoTxt()
        {
            FileStream f = new FileStream("archivoPiezas.txt", FileMode.Truncate);
            System.IO.StreamWriter file = new System.IO.StreamWriter(f);
            file.Close();

        }


        public static void piezasEnArchivo(int pieza) {
            FileStream f = new FileStream("archivoPiezas.txt", FileMode.Append); 
            System.IO.StreamWriter file = new System.IO.StreamWriter(f);
            file.WriteLine(pieza);
            file.Close();
        }

       

        public static  void lecturaPiezas() {
            string dato;
            arregloPiezasTxt = new string[2000];
            leerTxt = new StreamReader("archivoPiezas.txt");

            for (int i = 0; !leerTxt.EndOfStream; i++)
            {
                arregloPiezasTxt[i] = leerTxt.ReadLine();
                dato = arregloPiezasTxt[i];
                piezasArchivo.Add(Convert.ToInt32(dato));
            }

            leerTxt.Close();
           // imprimirArregloPiezasTxt(piezasArchivo);
        }

        public static void imprimirArregloPiezasTxt(ArrayList arreglo)
        {
            Console.Write("\n" + "Elementos en el archivo y en el arreglo: "+"\n");
            foreach (int element in arreglo)
            {
                Console.Write(element +" ");
            }
            Console.ReadKey();
        }
        
        
        /*Metodo encargado de escoger el tamaño de la matriz*/
        public static void tamañoDeMatriz(int i, int j) {
            
            matriz = new int[i, j];
            matriz2 = new int[i, j];
        
        }

     
        /*funcion encargada de verificar si el campo de la figura verde a insertar esta en 0 es decir desocupado, además hace comprobacion de las
         restricciones de la matriz*/

        public static Boolean comprobarfiguraVerde(int iBase, int jBase)
        {
            
            if ((iBase>=2 && jBase < matriz.GetLength(0) - 1))
            {
                return ((matriz[iBase, jBase + 1] == 0) && (matriz[iBase - 1, jBase + 1] == 0) && (matriz[iBase - 1, jBase] == 0) && (matriz[iBase - 2, jBase] == 0));
            }
            else
            {
                return false;
            }

        }


        /*funcion encargada de verificar si el campo de la figura verde a insertar esta en 0 es decir desocupado, además hace comprobacion de las
    restricciones de la matriz*/

        public static Boolean comprobarfiguraVerdeZ(int iBase, int jBase)
        {

            if ((iBase > 0 && jBase < matriz.GetLength(0) - 2))
            {
                return ((matriz[iBase, jBase + 1] == 0) && (matriz[iBase, jBase + 2] == 0) && (matriz[iBase - 1, jBase] == 0) && ( matriz[iBase - 1, jBase + 1] == 0));
            }
            else
            {
                return false;
            }

        }




        /*funcion encargada de verificar si el campo de la LArriba a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarLArriba(int iBase, int jBase)
        {

            if ((iBase >= 1 && jBase < matriz.GetLength(0) - 2))
            {
                return ((matriz[iBase, jBase] == 0) && ( matriz[iBase, jBase + 1]  == 0) && ( matriz[iBase, jBase + 2] == 0) && ( matriz[iBase - 1, jBase + 2] == 0));

            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de Lizquierda a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarLIzquierda(int iBase, int jBase)
        {

            if ((iBase >= 2 && jBase < matriz.GetLength(0) - 1))
            {
                return ((matriz[iBase, jBase] == 0) && (matriz[iBase, jBase + 1] == 0) && (matriz[iBase - 1, jBase] == 0) && (matriz[iBase - 2, jBase] == 0));

            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de la Lderecha a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarLderecha(int iBase, int jBase)
        {

            if ((iBase >= 2 && jBase < matriz.GetLength(0) - 1))
            {
                return ((matriz[iBase, jBase] == 0) && ( matriz[iBase, jBase + 1] == 0) && (matriz[iBase - 1, jBase] == 0) && (matriz[iBase - 2, jBase] == 0));
           
            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de Labajo a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/

        public static Boolean comprobarLAbajo(int iBase, int jBase)
        {

            if ((iBase >= 1 && jBase < matriz.GetLength(0) - 2))
            {
                return (( matriz[iBase, jBase] == 0) && ( matriz[iBase - 1, jBase] == 0) && ( matriz[iBase - 1, jBase + 1] == 0) && ( matriz[iBase - 1, jBase + 2] == 0));
            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo del rectangulo verticual a insertar esta en 0 es decir desocupado, además hace comprobacion de las
   restricciones de la matriz*/

        public static Boolean comprobarRectanguloV(int iBase, int jBase)
        {
            if ((iBase >= 3))
            {
                return ((matriz[iBase, jBase] == 0) && (matriz[iBase - 1, jBase] == 0) && (matriz[iBase - 2, jBase] == 0) && (matriz[iBase - 3, jBase] == 0));
            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo del rectangulo horizontal a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/

        public static Boolean comprobarRectanguloH(int iBase, int jBase)
        {
            if ((jBase < matriz.GetLength(0) - 3))
            {
                return ((matriz[iBase, jBase] == 0) && (matriz[iBase, jBase+1] == 0) && (matriz[iBase, jBase+2] == 0) && (matriz[iBase, jBase+3] == 0));
            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de la TAbajo a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarTAbajo(int iBase, int jBase)
        {
            if ((iBase > 0 && jBase < matriz.GetLength(0) - 2))
            {
                return (( matriz[iBase, jBase + 1]== 0) && ( matriz[iBase - 1, jBase] == 0) && (matriz[iBase - 1, jBase + 1]  == 0) && ( matriz[iBase - 1, jBase + 2] == 0));
            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de la T arriba a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
         public static Boolean comprobarTArriba(int iBase, int jBase)
        {
            if ((iBase >=1 && jBase < matriz.GetLength(0) - 2))
            {
                return ((  matriz[iBase, jBase]== 0) && ( matriz[iBase, jBase+1] == 0) && ( matriz[iBase, jBase+2]  == 0) && (  matriz[iBase-1, jBase+1] == 0));
            }
            else
            {
                return false;
            }

        }

         /*funcion encargada de verificar si el campo de la Tizquierda a insertar esta en 0 es decir desocupado, además hace comprobacion de las
       restricciones de la matriz*/
        
         public static Boolean comprobarTIzquierda(int iBase, int jBase)
         {
             if ((iBase >= 2 && jBase < matriz.GetLength(0) - 1))
             {
                 return ((matriz[iBase, jBase + 1] == 0) && (matriz[iBase - 1, jBase + 1] == 0) && (matriz[iBase - 2, jBase + 1] == 0) && (matriz[iBase - 1, jBase] == 0));
             }
             else
             {
                 return false;
             }

         }

         /*funcion encargada de verificar si el campo de tDerecha a insertar esta en 0 es decir desocupado, además hace comprobacion de las
       restricciones de la matriz*/
         public static Boolean comprobarTDerecha(int iBase, int jBase)
         {
             if ((iBase >= 2 && jBase < matriz.GetLength(0) - 1))
             {
                 return (( matriz[iBase, jBase] == 0) && (matriz[iBase - 1, jBase] == 0) && ( matriz[iBase - 2, jBase] == 0) && (matriz[iBase - 1, jBase + 1] == 0));
             
             }
             else
             {
                 return false;
             }

         }

         /*funcion encargada de verificar si el campo del cuadrado a insertar esta en 0 es decir desocupado, además hace comprobacion de las
       restricciones de la matriz*/
        public static Boolean comprobarCuadrado(int iBase,int jBase) {
            if ((iBase> 0 && jBase < matriz.GetLength(0) - 1))
            {
                return ((matriz[iBase, jBase] == 0) && (matriz[iBase, jBase + 1] == 0) && (matriz[iBase - 1, jBase] == 0) && ( matriz[iBase - 1, jBase + 1] == 0));
            
            
            }
            else {
                return false;
            }
           
        }

       







        /*funcion encargada de dar estructura a la figura del cuadrado que se desea crear*/
        public static void cuadrado(int iBase,int jBase)
        {
           
            matriz[iBase, jBase] = 2;
            matriz[iBase, jBase+1] = 2;
            matriz[iBase-1, jBase] = 2;
            matriz[iBase-1, jBase+1] = 2;
            
        }

        /*funcion encargada de dar estructura a la figura de la TAbajo que se desea crear*/
        public static void tAbajo(int iBase, int jBase) {
            
            matriz[iBase, jBase+1] = 9;
            matriz[iBase-1, jBase] = 9;
            matriz[iBase-1, jBase+1] = 9;
            matriz[iBase-1, jBase + 2] = 9;

            
        }

        /*funcion encargada de dar estructura a la figura del tArriba que se desea crear*/

        public static void tArriba(int iBase, int jBase)
        {
        
            matriz[iBase, jBase] = 9;
            matriz[iBase, jBase+1] = 9;
            matriz[iBase, jBase+2] = 9;
            matriz[iBase-1, jBase+1] = 9;

        }


        /*funcion encargada de dar estructura a la figura de tIzquierda que se desea crear*/
        public static void tIzquierda(int iBase, int jBase)
        {

            matriz[iBase, jBase+1] = 9;
            matriz[iBase-1, jBase+1] = 9;
            matriz[iBase-2, jBase+1] = 9;
            matriz[iBase-1, jBase] = 9;

        }

        /*funcion encargada de dar estructura a la figura de tDerecha que se desea crear*/
        public static void tDerecha(int iBase, int jBase)
        {
            
            matriz[iBase, jBase] =9;
            matriz[iBase-1, jBase] = 9;
            matriz[iBase - 2, jBase] = 9;
            matriz[iBase-1, jBase+1] = 9;

        }

        /*funcion encargada de dar estructura a la figura del rectangulo Horizontal que se desea crear*/

        public static void rectanguloHorizontal(int iBase, int jBase) {

            matriz[iBase, jBase] = 4;
            matriz[iBase,jBase+1] = 4;
            matriz[iBase,jBase+ 2] = 4;
            matriz[iBase, jBase+3] = 4;
            
        
        }


        /*funcion encargada de dar estructura a la figura de la lArriba que se desea crear*/
        public static void lArriba(int iBase, int jBase)
        {
            matriz[iBase, jBase] = 3;
            matriz[iBase, jBase+1] = 3;
            matriz[iBase, jBase+2] = 3;
            matriz[iBase-1, jBase+2] = 3;
        }


        /*funcion encargada de dar estructura a la figura de la lAbajo que se desea crear*/
        public static void lAbajo(int iBase, int jBase)
        {
            matriz[iBase, jBase] = 3;
            matriz[iBase-1, jBase] = 3;
            matriz[iBase-1, jBase+1] = 3;
            matriz[iBase - 1, jBase+2] = 3;
        }


        /*funcion encargada de dar estructura a la figura de lizquierda que se desea crear*/
        public static void lizquierda(int iBase, int jBase)
        {
            matriz[iBase, jBase+1] =3 ;
            matriz[iBase-1, jBase+1] =3;
            matriz[iBase-2, jBase+1] = 3;
            matriz[iBase-2, jBase] = 3;
        }

        /*funcion encargada de dar estructura a la figura de la lderecha que se desea crear*/
        public static void lDerecha(int iBase, int jBase)
        {
            matriz[iBase, jBase] = 3;
            matriz[iBase, jBase+1] = 3;
            matriz[iBase-1, jBase] = 3;
            matriz[iBase-2, jBase] = 3;
        }

        /*funcion encargada de dar estructura a la figura de rectanguloVertical que se desea crear*/
        public static void rectanguloVertical(int iBase, int jBase)
        {

            matriz[iBase, jBase] = 4;
            matriz[iBase-1, jBase] = 4;
            matriz[iBase-2, jBase] = 4;
            matriz[iBase-3, jBase] = 4;
        }

        /*funcion encargada de dar estructura a la figura verde posicion 1 que se desea crear*/
        public static void figuraVerdePosc1(int iBase, int jBase)
        {
            
                 matriz[iBase, jBase+1] = 5;
                 matriz[iBase-1, jBase+1] = 5;
                 matriz[iBase-1, jBase] = 5;
                 matriz[iBase - 2, jBase] = 5;
               
           
        }

        /*funcion encargada de dar estructura a la figura verde que es una Z que se desea crear*/
        public static void figuraVerdeZ(int iBase, int jBase)
        {
            matriz[iBase, jBase+1] = 6;
            matriz[iBase, jBase+2] = 6;
            matriz[iBase-1, jBase] = 6;
            matriz[iBase-1, jBase+1] = 6; 
        }

        

        /*funcion encargada de dar color a las figuras en la consola*/
        public static void colores() {
            uint STD_OUTPUT_HANDLE = 0xfffffff5;
            IntPtr hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

            for (int k = 1; k < 50; k++)
            {
                SetConsoleTextAttribute(hConsole, k);
                Console.WriteLine("Press Enter to exit ...");

            }
            Console.Read();  // wait
        }

        public static void imprimirMatriz(int[,] pMatriz)
        {

            uint STD_OUTPUT_HANDLE = 0xfffffff5;
            IntPtr hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
           
       
            for (int i = 0; i < pMatriz.GetLength(0); i++)
            {
             
                for (int j = 0; j < pMatriz.GetLength(0); j++)
                {
                    SetConsoleTextAttribute(hConsole, getColor(pMatriz[i, j]));
                    Console.Write(pMatriz[i,j]);
                    
                }
                Console.Write("\n");
            }
            Console.ReadKey();
        }


      /*funcion encargada de cambiar el color de el elemento a pintar en matriz*/
        public static int getColor(int value)
        {
            if (value == 2)
            {
                return 14;
            }
            if (value == 5)
            {
                return 2;
            }


            if (value == 3)
            {
                return 4;
            }

            if (value == 4)
            {
                return 3;
            }

            if (value == 9)
            {
                return 5;
            }
            if (value == 6)
            {
                return 6;
            }

            return 15;
        }

        /*funcion encargada de crear la matriz inicial en 0*/
        public static void matrizInicial(){
             
     
            for (int i = 0; i < matriz.GetLength(0); i++) {
                for (int j = 0; j < matriz.GetLength(0); j++)
                {
                    matriz[i,j]= 0;
                    matriz2[i, j] = 0;
                }
            }
         //   imprimirMatriz(matriz);
        }


        /*funcion encargada indicar la cantidad de memoria utilizada*/
        public static void cantidadMemoriaUtilizada(){
        
        long memory = GC.GetTotalMemory(true);
            Console.Write(" Espacio de memoria Ram: " + memory / 1024 + "kB"+"\n");
            
        }

        /*funcion creada para almacenar todos los datos del algoritmo voraz*/
        public static void correrVoraz(int tamañoM, int cantidadPiezas)
        {
            espacios();
            Console.WriteLine("      ALGORITMO VORAZ    ");
            espacios();
            tamañoDeMatriz(tamañoM,tamañoM);
            matrizInicial();
            Voraz.cargarArregloPiezas();
            Voraz.colocarPieza();
            imprimirMatriz(matriz);
            espacios();
            Console.Write("\n" + " Cant. Figuras Generadas Inicialmente: "  + cantidadPiezas+ "\n");
            Console.Write("\n" + " Cant. Asignaciones : " + Voraz.Asignaciones + "\n");
            Console.Write("\n" + " Cant.Comprobaciones " + Voraz.comprobacionesVoraz + "\n");
            Console.Write("\n" + " Cant. Lineas ejecutadas: " + Voraz.lineasEvaluadasVoraz + "\n"+"\n");
            cantidadMemoriaUtilizada();
            
        }

        /*funcion creada para almacenar todos los datos del algoritmo Branch and Bound*/
        public static void correrBranchBound(int tamañoM, int cantidadPiezas)
        {
            espacios();
            Console.WriteLine("      ALGORITMO BRANCH & BOUND    ");
            espacios();
            tamañoDeMatriz(tamañoM,tamañoM);
            matrizInicial();
            BranchBound.cargarArregloPiezas();
            BranchBound.colocarPieza();
            imprimirMatriz(matriz);
            espacios();
            Console.Write("\n" + " Figuras Generadas Inicialmente: " + cantidadPiezas + "\n");
            Console.Write("\n" + " Cant. Asignaciones : " + BranchBound.AsignacionesPoda +"\n");
            Console.Write("\n" + " Comprobaciones " + BranchBound.comprobacionesPoda + "\n");
            Console.Write("\n" + " Cant. Lineas evaluadas: " + BranchBound.lineasEjecutadasPoda + "\n"+"\n");
            cantidadMemoriaUtilizada();
           
            
        }

        /*funcion creada para almacenar todos los datos del algoritmo Backtracking*/
        public static void correrBrackTracking(int tamañoM, int cantidadPiezas)
        {
            espacios();
            Console.WriteLine("      ALGORITMO BackTracking    ");
            espacios();
            tamañoDeMatriz( tamañoM, tamañoM);
            matrizInicial();
            Backtracking.piezas.Clear();
            Backtracking.cargarArregloPiezas();
            Backtracking.colocarPieza();
            imprimirMatriz(matriz);
            espacios();
            Console.Write("\n" + " Figuras Generadas Inicialmente: " + cantidadPiezas + "\n");
            Console.Write("\n" + " Cant. Asignaciones : " +Backtracking.asignacionesBackTracking+ "\n");
            Console.WriteLine("\n Comprobaciones " + Backtracking.comprobacionesBacktracking + "\n");
            Console.Write("\n" + " Cant. Lineas ejecutadas: " + Backtracking.lineasEjecutadasBacktracking+ "\n" + "\n");

            cantidadMemoriaUtilizada();


        }

        public static void correrGenetico(int tamañoM,int cantidadPiezas)
        {
            espacios();
            Console.WriteLine("      ALGORITMO Genetico    ");
            espacios();
            Console.Write("\n\n         Matriz 1       \n\n");
            espacios();
            tamañoDeMatriz(tamañoM,tamañoM);
            matrizInicial();
            Genetico.listaPiezasEnMatriz1.Clear();
            Genetico.colocarPieza(1);
            imprimirMatriz(matriz);
            espacios();
           
            Console.Write("\n\n         Matriz 2       \n\n");
            espacios();
            tamañoDeMatriz(tamañoM, tamañoM);
            matrizInicial();
            Genetico.listaPiezasEnMatriz2.Clear();
            Genetico.colocarPieza(2);
            imprimirMatriz(matriz);
            espacios();
            Genetico.crearMatriz3(tamañoM);
            Console.Write("\n" + " Figuras Generadas Inicialmente: " + cantidadPiezas + "\n");
            Console.Write("\n" + " Cant. Asignaciones : " + Genetico.AsignacionesGenetico + "\n");
            Console.Write("\n" + " Comprobaciones " +      Genetico.comprobacionesGenetico + "\n");
            Console.Write("\n" + " Cant. Lineas evaluadas: " + Genetico.lineasEjecutadasGenetico + "\n" + "\n");
            cantidadMemoriaUtilizada();
            Genetico.imprimirCruces();
            
        }


     

        static void Main(string[] args)
        {
            
            menuInicial();
            Console.ReadKey();
        }



        public static void reiniciarAsigComp()
        {
                        Backtracking.asignacionesBackTracking = 0;
                        Backtracking.comprobacionesBacktracking = 0;
                        BranchBound.AsignacionesPoda = 0;
                        BranchBound.comprobacionesPoda = 0;
                        Genetico.AsignacionesGenetico = 0;
                        Genetico.comprobacionesGenetico = 0;
                        Voraz.Asignaciones = 0;
                        Voraz.comprobacionesVoraz = 0;
        }



        public static void menuInicial() {

            int cantidadVeces =0;
            while (cantidadVeces < 5)
            {
                espacios();
                espacios();
                Console.Write("Digite la cantidad de piezas desea probar\n" +
                "1. Piezas 10\n" + "2. Piezas 100 " + "\n" + "3: Piezas 1000\n" + "4: Piezas 2000" + "\n Opcion: ");
                int tamañoMatriz = int.Parse(Console.ReadLine());
                switch (tamañoMatriz)
                {
                    case 1:

                        reiniciarAsigComp();
                        limpiarArchivoTxt();
                        generacionPiezasInicial(10);
                        lecturaPiezas();
                        espacios();
                        crono2 = Stopwatch.StartNew();
                        correrVoraz(7, 10);
                        long Milisegundos2 = crono2.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos2 + " Milisegundos");
                        espacios();

                        crono2 = Stopwatch.StartNew();
                        correrBrackTracking(6, 10);
                        Milisegundos2 = crono2.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos2 + " Milisegundos");
                        espacios();

                        crono2 = Stopwatch.StartNew();
                        correrBranchBound(6, 10);
                        Milisegundos2 = crono2.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos2 + " Milisegundos");
                        espacios();


                        crono2 = Stopwatch.StartNew();
                        correrGenetico(6, 10);
                        Milisegundos2 = crono2.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos2 + " Milisegundos");
                        cantidadVeces++;
                        espacios();
                        espacios();
                        break;


                    case 2:

                        reiniciarAsigComp();
                        limpiarArchivoTxt();
                        generacionPiezasInicial(100);
                        lecturaPiezas();
                        espacios();

                        crono1 = Stopwatch.StartNew();
                        correrVoraz(21, 100);
                        long Milisegundos1 = crono1.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos1 + " Milisegundos");
                        espacios();

                        crono1 = Stopwatch.StartNew();
                        correrBrackTracking(21, 100);
                        Milisegundos1 = crono1.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos1 + " Milisegundos");
                        espacios();

                        crono1 = Stopwatch.StartNew();
                        correrBranchBound(21, 100);
                        Milisegundos1 = crono1.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos1 + " Milisegundos");
                        espacios();


                        crono1 = Stopwatch.StartNew();
                        correrGenetico(21, 100);
                        Milisegundos1 = crono1.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos1 + " Milisegundos");
                        cantidadVeces++;
                        break;

                    case 3:

                        reiniciarAsigComp();
                        limpiarArchivoTxt();
                        generacionPiezasInicial(1000);
                        lecturaPiezas();
                        espacios();

                        crono3 = Stopwatch.StartNew();
                        correrVoraz(71, 1000);
                        long Milisegundos3 = crono3.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos3 + " Milisegundos");
                        espacios();

                        crono3 = Stopwatch.StartNew();
                        correrBrackTracking(71, 1000);
                        Milisegundos3 = crono3.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos3 + " Milisegundos");
                        espacios();

                        crono3 = Stopwatch.StartNew();
                        correrBranchBound(71, 1000);
                        Milisegundos3 = crono3.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos3 + " Milisegundos");
                        espacios();


                        crono3 = Stopwatch.StartNew();
                        correrGenetico(71, 1000);
                        Milisegundos3 = crono3.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos3 + " Milisegundos");
                        cantidadVeces++;
                        break;

                

                case 4:

                        reiniciarAsigComp();
                        limpiarArchivoTxt();
                        generacionPiezasInicial(2000);
                        lecturaPiezas();
                        espacios();

                        crono4 = Stopwatch.StartNew();
                        correrVoraz(80, 2000);
                        long Milisegundos4 = crono4.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos4 + " Milisegundos");
                        espacios();

                        crono4 = Stopwatch.StartNew();
                        correrBrackTracking(80, 2000);
                        Milisegundos4 = crono4.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos4 + " Milisegundos");
                        espacios();

                        crono4 = Stopwatch.StartNew();
                        correrBranchBound(80, 2000);
                        Milisegundos4 = crono4.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos4 + " Milisegundos");
                        espacios();


                        crono4 = Stopwatch.StartNew();
                        correrGenetico(80, 2000);
                        Milisegundos3 = crono4.ElapsedMilliseconds;
                        Console.WriteLine("\n Tiempo: " + Milisegundos4 + " Milisegundos");
                        cantidadVeces++;
                        break;

                }

               }

            
        }


        public static void espacios() {
            Console.Write("\n\n"); 
        }


    }
}

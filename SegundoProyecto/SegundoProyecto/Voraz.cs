﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegundoProyecto
{
    class Voraz
    {
        public static int comprobacionesVoraz = 0;
        public static Random randomGenerator = new Random();
        public static ArrayList piezas = new ArrayList();
        static int piezaResultadoGeneradorP = 0;
        public static int Asignaciones =0;
        public static int lineasEvaluadasVoraz = 0;
       


       
        /*metodo encargado de ingresar las piezas al arraylist Piezas, en total ingresara 13*/
        public static void cargarArregloPiezas()
        {
            piezas.Clear();
            lineasEvaluadasVoraz += 1;
            for (int ingresarNumeros=0 ; ingresarNumeros < Program.piezasArchivo.Count; ingresarNumeros++)
            {
                lineasEvaluadasVoraz += 2;
                piezas.Add(ingresarNumeros);
            }
        }


        /*Metodo encargado de generar piezas desde 1 a 13*/
        public static int generadorPieza()
        {
           
            int p = randomGenerator.Next(0, piezas.Count);
            lineasEvaluadasVoraz += 2;
            return (int)piezas[p];
           
        }

     

       /*Metodo encargado de verificar la disponibilidad de la pieza, en caso que sea disponible su campo retorna la pieza, y en caso de que
        * no lo sea intenta colocarla en otra posición en la matriz y que este disponible**/
        public static int ObtenerPieza(int i, int j)
        {
            lineasEvaluadasVoraz = 1;
            bool disponibilidadCampo;

            while(true)
            {
                
               if(piezas.Count == 0){
                   lineasEvaluadasVoraz += 2;
                    break;
                }

                piezaResultadoGeneradorP = generadorPieza();
                disponibilidadCampo = false;
                lineasEvaluadasVoraz += 2;

                if (piezaResultadoGeneradorP == 1)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarCuadrado(i, j);
                    lineasEvaluadasVoraz += 3;
                }

                else if (piezaResultadoGeneradorP == 2)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarRectanguloV(i, j);
                    lineasEvaluadasVoraz += 3;
                }

                else if (piezaResultadoGeneradorP == 3)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarfiguraVerde(i, j);
                    lineasEvaluadasVoraz += 3;
                }

                else if (piezaResultadoGeneradorP == 4)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarRectanguloH(i, j);
                    lineasEvaluadasVoraz += 3;
                }

                else if (piezaResultadoGeneradorP == 5)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLderecha(i, j);
                    lineasEvaluadasVoraz += 3;

                }
                else if (piezaResultadoGeneradorP == 6)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTAbajo(i, j);
                    lineasEvaluadasVoraz += 3;
                }
                else if (piezaResultadoGeneradorP == 7)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLAbajo(i, j);
                    lineasEvaluadasVoraz += 3;
                }
                else if (piezaResultadoGeneradorP == 8)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLIzquierda(i, j);
                    lineasEvaluadasVoraz += 3;
                }
                else if (piezaResultadoGeneradorP == 9)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLArriba(i, j);
                    lineasEvaluadasVoraz+= 3;
                }
                else if (piezaResultadoGeneradorP == 10)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTArriba(i, j);
                    lineasEvaluadasVoraz += 3;
                }
                else if (piezaResultadoGeneradorP == 11)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTDerecha(i, j);
                    lineasEvaluadasVoraz += 3;
                }
                else if (piezaResultadoGeneradorP == 12)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTIzquierda(i, j);
                    lineasEvaluadasVoraz += 3;
                }
                else if (piezaResultadoGeneradorP == 13)
                {
                    comprobacionesVoraz++;
                    disponibilidadCampo = ComprobacionFigura.comprobarfiguraVerdeZ(i, j);
                    lineasEvaluadasVoraz += 3;
                }

                if (disponibilidadCampo == false)
                {
                   
                    piezas.Remove(piezaResultadoGeneradorP);
                    lineasEvaluadasVoraz += 3;
                }
                else
                {
                    lineasEvaluadasVoraz += 1;
                    return piezaResultadoGeneradorP;
                    
                }
            }
            return -1;
        }


        /*Colocar Pieza solicita una pieza y verifica su disponibilidad el resultado de ese proceso lo aprovecha y coloca la pieza donde corresponda
         una vez evaluada su disponibilida de campo y estado de la pieza*/
       
        public static void colocarPieza()
        {
            lineasEvaluadasVoraz += 6; // variables globales
            int length = Program.matriz.GetLength(0);
            for (int i = length - 1; i >= 0; i--)
            {
                lineasEvaluadasVoraz+=2;
                for (int j = 0; j < length; j++)
                {
                    cargarArregloPiezas();
                    int figura = ObtenerPieza(i, j);
                    lineasEvaluadasVoraz+= 2;   
         

                    if (figura == 1)
                    {
                        Program.cantidadFigurasGeneradas++;
                        Program.cuadrado(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }
                    else if (figura == 2)
                    {
                        Program.cantidadFigurasGeneradas++;
                        Program.rectanguloVertical(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }

                    else if (figura == 3)
                    {

                        Program.cantidadFigurasGeneradas++;
                        Program.figuraVerdePosc1(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;

                    }
                    else if (figura == 4)
                    {

                        Program.cantidadFigurasGeneradas++;
                        Program.rectanguloHorizontal(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;

                    }
                    else if (figura == 5)
                    {

                        Program.cantidadFigurasGeneradas++;
                        Program.lDerecha(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }


                    else if (figura == 6)
                    {

                        Program.cantidadFigurasGeneradas++;
                        Program.tAbajo(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }
                    else if (figura == 7)
                    {

                        Program.cantidadFigurasGeneradas++;
                        Program.lAbajo(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }

                    else if (figura == 8)
                    {

                        Program.cantidadFigurasGeneradas++;
                        Program.lizquierda(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }
                    else if (figura == 9)
                    {
                        Program.cantidadFigurasGeneradas++;
                        Program.lArriba(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }

                    else if (figura == 10)
                    {

                        Program.cantidadFigurasGeneradas++;
                        Program.tArriba(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }
                    else if (figura == 11)
                    {

                        Program.cantidadFigurasGeneradas++;
                        Program.tDerecha(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }
                    else if (figura == 12)
                    {

                        Program.cantidadFigurasGeneradas++;
                        Program.tIzquierda(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }

                    else if (figura == 13)
                    {

                        Program.cantidadFigurasGeneradas++;
                        Program.figuraVerdeZ(i, j);
                        Asignaciones++;
                        lineasEvaluadasVoraz += 4;
                    }
                }

            }

        }

    
    }
}

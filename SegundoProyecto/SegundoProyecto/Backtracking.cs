﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegundoProyecto
{
    class Backtracking
    {
        //Variable que almacena las comprobaciones realizadas en el algortimo BackTracking
        public static int comprobacionesBacktracking= 0;

        //Variable que almacena las asignaciones realizadas en el algortimo BackTracking
        public static int asignacionesBackTracking = 0;


        //Variable que almacena la cantidad de lineas recorridas en una ejecucion
        public static int lineasEjecutadasBacktracking = 0;
        
        //Libreria para generar un random
        static Random randomGenerator = new Random();
        
        //Arreglo que almacena las piezas inicialmente guardadas en el archivo
        public static ArrayList piezas = new ArrayList();

        //Arreglo que se modifica temporalmente, inicialmente cuenta con todas las posibilidades pero se van reduciendo
        public static ArrayList piezasDisponibles = new ArrayList();



        
        /*Funcion creada para cargar las piezas que se guardan en el archivo, para trabajar sobre el arreglo piezas en la clase poda*/
        public static void cargarArregloPiezas(){
             piezas.Clear();
             for (int ingresarNumeros = 0; ingresarNumeros < Program.piezasArchivo.Count; ingresarNumeros++) {
                piezas.Add(ingresarNumeros);
                lineasEjecutadasBacktracking += 3;
            }
        }

        /*Funcion encargada de verificar disponibilidad del campo, comprobando con cada pieza que hay en el arreglo piezas del archivo*/
        public static void seleccionarCandidatos(int i, int j)
        {
            bool disponibilidadCampo;
          
           
            for (int k = 0 ; k < piezas.Count; k++)
            {
                disponibilidadCampo = false;

                if (k == 1)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarCuadrado(i, j);
                }

                else if (k == 2)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarRectanguloV(i, j);

                }

                else if (k == 3)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarfiguraVerde(i, j);
                }

                else if (k == 4)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarRectanguloH(i, j);
                }

                else if (k == 5)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLderecha(i, j);

                }
                else if (k == 6)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTAbajo(i, j);
                }
                else if (k == 7)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLAbajo(i, j);
                }
                else if (k == 8)
                {
                  lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLIzquierda(i, j);
                }
                else if (k == 9)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLArriba(i, j);
                }
                else if (k == 10)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTArriba(i, j);
                }
                else if (k == 11)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTDerecha(i, j);
                }
                else if (k == 12)
                {
                    lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTIzquierda(i, j);
                }
                else if (k == 13)
                {
                   lineasEjecutadasBacktracking += 4;
                    comprobacionesBacktracking++;
                    disponibilidadCampo = ComprobacionFigura.comprobarfiguraVerdeZ(i, j);
                    
                }

                if (disponibilidadCampo)
                {
                    lineasEjecutadasBacktracking += 2;
                    piezasDisponibles.Add(k);
                }
                
            }
        }


        public static int obtener() {
            int var = 0;
            for (int i = 0; i < piezasDisponibles.Count; i++)
            {
                var= (int)piezasDisponibles[i];
            }
            return var;
        }

        public static void colocarPieza()
        {
            lineasEjecutadasBacktracking += 5;// variables globales
            int length = Program.matriz.GetLength(0);
            
            for (int i = length - 1; i >= 0; i--)
            {
                lineasEjecutadasBacktracking += 1;
                for (int j = 0; j < length; j++)
                {
                    piezasDisponibles.Clear();
                    cargarArregloPiezas();
                    seleccionarCandidatos(i,j);
                    int figura = obtener();
                    
                    if (figura == 1)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.cuadrado(i, j);



                    }
                    else if (figura == 2)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.rectanguloVertical(i, j);

                    }


                    else if (figura == 3)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.figuraVerdePosc1(i, j);


                    }
                    else if (figura == 4)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.rectanguloHorizontal(i, j);


                    }
                    else if (figura == 5)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.lDerecha(i, j);
                    }

                    else if (figura == 6)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.tAbajo(i, j);


                    }
                    else if (figura == 7)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.lAbajo(i, j);


                    }

                    else if (figura == 8)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.lizquierda(i, j);


                    }
                    else if (figura == 9)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.lArriba(i, j);


                    }

                    else if (figura == 10)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.tArriba(i, j);


                    }
                    else if (figura == 11)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.tDerecha(i, j);


                    }
                    else if (figura == 12)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.tIzquierda(i, j);


                    }

                    else if (figura == 13)
                    {
                        lineasEjecutadasBacktracking += 5;
                        asignacionesBackTracking += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.figuraVerdeZ(i, j);


                    }

                }
                    
            }
                     
        
        }
    }
}

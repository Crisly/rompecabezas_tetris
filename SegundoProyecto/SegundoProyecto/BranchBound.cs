﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegundoProyecto
{
    class BranchBound
    {
        /* Variables globales*/
        public static int comprobacionesPoda = 0; // almacena las cantidad de comprobaciones realizadas sean True o False
        public static int   AsignacionesPoda = 0; // almacena las asignaciones de piezas que hace en matriz
        
        //funcion para generar random
        public static Random randomGenerator = new Random();
 
        //arreglo que almacena las piezas con mayor ponderacion
        public static ArrayList piezasDisponibles = new ArrayList();

        //Arreglo que almacena las piezas que estan en el archivo inicialmente
        public static ArrayList piezas = new ArrayList();

        //variable que permite el conteo de total de lineas ejecutadas
        public static int lineasEjecutadasPoda=0;




       
        /*Funcion creada para cargar las piezas que se guardan en el archivo, para trabajar sobre el arreglo piezas en la clase poda*/
        public static void cargarArregloPiezas(){
             piezas.Clear();
             for (int ingresarNumeros = 0; ingresarNumeros < Program.piezasArchivo.Count; ingresarNumeros++) {
                piezas.Add(ingresarNumeros);
                lineasEjecutadasPoda += 3;
            }
        }

        /*Funcion encargada de verificar disponibilidad del campo para escribir la pieza*/
        public static void seleccionarCandidatos(int i, int j)
        {
            bool disponibilidadCampo;
          
           
            for (int k = 0 ; k < piezas.Count; k++)
            {
                disponibilidadCampo = false;

                if (k == 1)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarCuadrado(i, j);
                }

                else if (k == 2)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarRectanguloV(i, j);

                }

                else if (k == 3)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarfiguraVerde(i, j);
                }

                else if (k == 4)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarRectanguloH(i, j);
                }

                else if (k == 5)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLderecha(i, j);

                }
                else if (k == 6)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTAbajo(i, j);
                }
                else if (k == 7)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLAbajo(i, j);
                }
                else if (k == 8)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLIzquierda(i, j);
                }
                else if (k == 9)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLArriba(i, j);
                }
                else if (k == 10)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTArriba(i, j);
                }
                else if (k == 11)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTDerecha(i, j);
                }
                else if (k == 12)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTIzquierda(i, j);
                }
                else if (k == 13)
                {
                    lineasEjecutadasPoda += 4;
                    comprobacionesPoda++;
                    disponibilidadCampo = ComprobacionFigura.comprobarfiguraVerdeZ(i, j);
                    
                }

                if (disponibilidadCampo)
                {
                    lineasEjecutadasPoda += 2;
                    piezasDisponibles.Add(k);
                }
                
            }
        }

       

        /*Funcion creada para seleccionar las mejores piezas de acuerdo a sus ponderaciones, estas son tomadas deacuedo a los espacios en blanco
         según como inicialmente se dibuja en matriz */
        public static int ponderacionesPiezas(int figura) { 
                 
                 if (figura == 1 ||figura == 2||figura == 5||figura == 8||figura == 11||figura == 12 )
                    {
                        lineasEjecutadasPoda += 2;
                       return 4 ;
                    }
                if (figura == 3||figura == 4||figura == 6||figura == 7||figura == 10||figura == 13)
                    {
                        lineasEjecutadasPoda += 2;
                       return 3;
                    }
              else{
                  lineasEjecutadasPoda += 1;
                    return 2;
                }
        }

        /*Funcion encargada de elegir pieza, este metodo depende del anterior por motivo que se consulta la poderacion para saber
         * si la pieza es optima*/
        public static int elegirPieza() {

            lineasEjecutadasPoda += 2;
            int piezaElegida = -1;
            int ponderacionElegida = -1;

            foreach (int pieza in piezasDisponibles) {
                int ponderacion = ponderacionesPiezas(pieza);
                lineasEjecutadasPoda += 2;
                if (ponderacion > ponderacionElegida) {
                    ponderacionElegida = ponderacion;
                    piezaElegida = pieza;
                    lineasEjecutadasPoda += 4;
                }
             }
            lineasEjecutadasPoda += 1;
            return piezaElegida;
        }



        public static void colocarPieza()
        {
            lineasEjecutadasPoda += 5;// variables globales
            int length = Program.matriz.GetLength(0);
            
            for (int i = length - 1; i >= 0; i--)
            {
                lineasEjecutadasPoda += 1;
                for (int j = 0; j < length; j++)
                {
                    piezasDisponibles.Clear();
                    cargarArregloPiezas();
                    seleccionarCandidatos(i,j);
                    int figura = elegirPieza();
                    
                    if (figura == 1)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.cuadrado(i, j);



                    }
                    else if (figura == 2)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.rectanguloVertical(i, j);

                    }


                    else if (figura == 3)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.figuraVerdePosc1(i, j);


                    }
                    else if (figura == 4)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.rectanguloHorizontal(i, j);


                    }
                    else if (figura == 5)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.lDerecha(i, j);
                    }

                    else if (figura == 6)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.tAbajo(i, j);


                    }
                    else if (figura == 7)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.lAbajo(i, j);


                    }

                    else if (figura == 8)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.lizquierda(i, j);


                    }
                    else if (figura == 9)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.lArriba(i, j);


                    }

                    else if (figura == 10)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.tArriba(i, j);


                    }
                    else if (figura == 11)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.tDerecha(i, j);


                    }
                    else if (figura == 12)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.tIzquierda(i, j);


                    }

                    else if (figura == 13)
                    {
                        lineasEjecutadasPoda += 5;
                        AsignacionesPoda += 1;
                        Program.cantidadFigurasGeneradas++;
                        Program.figuraVerdeZ(i, j);


                    }

                }
                    
            }
                     
        }
       
    }
}
